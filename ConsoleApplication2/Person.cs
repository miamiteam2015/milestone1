﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{
    public class Person
    {
        private int teudatZeut;
        private string firstName, lastName, gender;

        public Person(int teudatZeut, string firstName, string lastName, string gender)
        {
            this.teudatZeut = teudatZeut;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
        }
        public Person() { }

        
        public int getID()
        {
            return teudatZeut;
        }

        public void setID(int teudatZeut)
        {
            this.teudatZeut = teudatZeut;
        }
       

        public string getFirstName()
        {
            return firstName;
        }


        public string getLasttName()
        {
            return lastName;
        }

        public string getGender()
        {
            return gender;
        }

      

        public void setFirstName(String firstName)
        {
            this.firstName = firstName;
        }


        public void setLastName(String lastName)
        {
            this.lastName = lastName;
        }

        public void getGender(string gender)
        {
            this.gender = gender;
        }


        
    }
}