﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{
    public class Department
    {
        private string name;


        private int departmentID;

              
        public Department(string name, int departmentID)
        {
            this.name = name;
            this.departmentID = departmentID;
         }

        public Department()
        {
            name = "";
            departmentID = 0;
        }
        public string getName()
        {
            return name;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public int getID()
        {
            return departmentID;
        }
        public void setID(int departmentID)
        {
            this.departmentID = departmentID;
        }

    }
}
