﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BL_backend
{
    public class ClubMember : Person
    {
        private List<int> transactionID=new List<int>();
        private int memberID;
        private DateTime dateOfBirth;
        private int p1;
        private int p2;
        private int p3;
        private string p4;
        private string p5;
        private string p6;


        public ClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender) : base(teudatZeut, firstName, lastName, gender)
        {
            this.memberID = memberID;
            this.dateOfBirth = dateOfBirth;

        }

        public ClubMember()
               : base(0, "", "", "")
        {
            memberID = 0;
            dateOfBirth = new DateTime(2015,1,1);
            

        }
        public ClubMember(int p1, int p2, int p3, string p4, string p5, string p6)
        {
            // TODO: Complete member initialization
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
        }


        public int getMemberId()
        {
            return memberID;
        }
        public void setMemberId(int MemberId)
        {
            this.memberID = MemberId;
        }

        public DateTime getDateOfBirth()
        {
            return dateOfBirth;
        }
        public void setDateOfBirth(DateTime dateOfBirth)
        {
            this.dateOfBirth = dateOfBirth;
        }
        public List<int> TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }


    }

}