﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{  
    public enum productTypes
        {
            Electronics, Furniture, Cleaning, None
        } ;

    public class Product
    {
        private string name;

        private productTypes Type { get; set; }

        public int InventoryID;

        private int location;

        private Boolean inStock;

        private int stockCount;

        public double Price;



        public Product(string name, productTypes type, int inventoryID, int location, Boolean inStock, int stockCount, double price)
        {
            this.name = name;
            this.Type = type;
            this.InventoryID = inventoryID;
            this.location = location;
            this.inStock = inStock;
            this.stockCount = stockCount;
            this.Price = price;
        }

        public Product ()
        {
            name = "";
            Type = productTypes.None;
            InventoryID = 0;
            location = 0;
            inStock = true;
            stockCount = 0;
            Price = 0;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }


     
        public int Location
        {
            get { return location; }
            set { location = value; }
        }

        public int getID()
        {
            return InventoryID;
        }
        public void setID(int inventoryID)
        {
            this.InventoryID = inventoryID;
        }

        public bool InStock
        {
            get { return inStock; }
            set { inStock = value; }
        }
        public int getSC(int inventoryID)
        {
            return stockCount;
        }
        public void setSC(int stockCount)
        {
            this.stockCount = stockCount;
        }
        public double getPrice()
        {
            return Price;
        }
        public void setProce(double price)
        {
            this.Price = price;
        }

    }
}
