﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BL_backend
{
   public enum PaymentMethod
    {
        Cash,
        Visa,
        MasterCard,
        AmericanExpress,
        Isracard,
        Check,
        Other
    }     
   public class Transaction
    {
        private int transactionID;
        private DateTime dateTime;
        private bool isAReturn;
        public List<SoldProduct> receipt = new List<SoldProduct>();
        public PaymentMethod paymentMethod {get; set; }


        public Transaction(int transactionID, DateTime dateTime, Boolean isAReturn, PaymentMethod paymentMethod)
        {
            this.transactionID = transactionID;
            this.dateTime = dateTime;
            this.isAReturn = isAReturn;
            this.paymentMethod = paymentMethod;
        }

       public Transaction ()
       {
           transactionID=0;
           dateTime= new DateTime(2015,1,1);
           isAReturn = false;
           paymentMethod = PaymentMethod.Other;


       }

        public int getID()
        {
            return transactionID;
        }

        public void setID(int transactionID)
        {
            this.transactionID = transactionID;
        }

       
        public DateTime getDateTime()
        {
            return dateTime;
        }

        public void setDateTime(DateTime dateTime)
        {
            this.dateTime = dateTime;
        }

        public bool getIsAReturn()
        {
            return isAReturn;
        }

       public void setIsAReturn (bool isAReturn)
        {
            this.isAReturn = isAReturn;
        }
       


    }
   public class SoldProduct
   {
       public SoldProduct(Product product)
       {
           ID = product.InventoryID;
           Price = product.Price;
       }

       public int ID {get; set;}
       public double Price {get; set;}

   }
}
