﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_backend;

namespace BL
{
    public interface IBL
    {
        

        bool FindProduct(int inventoryId);

        bool FindEmployeeByID(int teudatZeut);

        bool FindClubMemberByID(int teudatZeut);

        bool FindTransactionByID(int transactionID);

        bool FindDepartmentByID(int departemntID);

        bool FindUser(string userName, int Id);

        
        
        void AddnewProduct(string name, productTypes Type, int inveID, int location, bool inStock, int stockCount, double price);
       
        void deleteProduct(int inventoryId); 

        void updateProduct(int inventoryId, int stockCount); // not yet

        void AddNewEmployee( int salary, int departmentID, int supervisor, int teudatZeut, string firstName, string lastName, string gender );
       
        void deleteEmployee(int teudatZeut);
        
        void AddnewClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender);
       
        void deleteClubMember(int teudatZeut);
        
        void AddnewTransaction(int transactionID, DateTime dateTime, Boolean isAReturn, PaymentMethod paymentMethod);

        void deleteTransaction(int transactionID);

        void AddNewDepartment(int departmentID, string name);

        void deleteDepartment(int departmentID);
   
        List<Employee> GetAllEmployeesBySalaryRange(double salary1, double salary2);
       
        List<ClubMember> GetClubMemberListByDateOfBirth(DateTime Age1, DateTime Age2);

        List<Transaction> GetAllTransactionListByDate(DateTime date);

        List<Transaction> GetTransactionListByPaymentMethod(PaymentMethod PaymentMethod);

        List<ClubMember> GetAllClubMemberByClubID(int MemberID);

        List<Employee> GetAllEmployeeListByDepID(int dpID);

        List<Product> GetProductListByType(Enum type);

        List<Product> GetProductListByPrice(double price1, double price2);

        List<Department> GetDepartmentByName(string name);

        int GetStockCountOfProduct(int inventoryID);

        List<Product> GetAllProductsList();

        List<ClubMember> GetAllClubMembers();
       
        List<Employee> GetAllEmployees();
        
        List<Transaction> GetAllTransactions();

        List<Department> GetAllDepartments();
      

     
      
}
}