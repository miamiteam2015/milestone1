﻿using BL_backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BL
{
    public class product_BL : IBL
    {
       public IDAL itsDAL;
       public product_BL(IDAL dal)// note that the constructor expects an IDAL type (and not a specific implementaion specific)
        {
            itsDAL = dal;
        }



       public bool FindUser(string userName, int password)
       {
           return itsDAL.IsUserExist(new User(userName,password));
       }

         public bool FindProduct(int Id)
        {
            Product P = itsDAL.FindProductByID(Id);
            if (P.getID() == 0)
            {
                return false;
            }
            else return true;
        }

      public bool FindEmployeeByID(int teudatZeut)
        {
             Employee E = itsDAL.FindEmployeeByID(teudatZeut);
            if (((Person)E).getID() == 0)
            {
                return false;
            }
            else return true;
        }

         public bool FindClubMemberByID(int teudatZeut)
        {
            ClubMember C = itsDAL.FindClubMemberByID(teudatZeut);
            if(((Person)C).getID() == 0)
            {
                return false;
            }
            else return true;
        }

          public bool FindTransactionByID(int transactionID)
        {
            Transaction T = itsDAL.FindTransactionByID(transactionID);
            if (T.getID() == 0)
            {
                return false;
            }
            else return true;
        }

          public bool FindDepartmentByID(int departmentID)
          {
              Department D = itsDAL.FindDepartmentById(departmentID);
              if (D.getID() == 0)
              {
                  return false;
              }
              else return true;
          }

         public List<Product> GetAllProductsList()
        {
            return itsDAL.GetProductList();
        }

          public List<ClubMember> GetAllClubMembers()
        {
            return itsDAL.GetClubMemberList();
        }

         public List<Employee> GetAllEmployees()
        {
            return itsDAL.GetEmployeeList();
        }

         public List<Transaction> GetAllTransactions()
        {
            return itsDAL.GetTransactionList();
        }

         public List<Department> GetAllDepartments()
         {
             return itsDAL.GetDepartmentList();
         }

        public void AddnewProduct(string name, productTypes Type, int inveID, int location, bool inStock, int stockCount, double price)
        {
            List<Product> productList = new List<Product>();
            Product P = new Product(name,  Type,  inveID,  location,  inStock,  stockCount, price);
            itsDAL.AddProduct(P); 
        }

        public void AddnewClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender)
        {
            List<ClubMember> clunMemberList = new List<ClubMember>();
            ClubMember C = new ClubMember(memberID, dateOfBirth, teudatZeut, firstName, lastName, gender);
            itsDAL.AddClubMember(C); 
        }

          public void  AddnewTransaction(int transactionID, DateTime dateTime, Boolean isAReturn, PaymentMethod paymentMethod)
        {
            List<Transaction> transactionList = new List<Transaction>();
            Transaction T = new Transaction(transactionID,  dateTime,  isAReturn,  paymentMethod);
            itsDAL.AddTransaction(T); 
        }

        public void AddNewEmployee(int salary, int departmentID, int supervisor, int teudatZeut, string firstName, string lastName, string gender)
          {
              List<Employee> employeeList = new List<Employee>();
              Employee E = new Employee (salary, departmentID, supervisor, teudatZeut, firstName, lastName, gender);
              itsDAL.AddEmployee(E); 
          }
        public void deleteProduct(int inventoryId)
        {
            Product P = itsDAL.FindProductByID(inventoryId);
            itsDAL.deleteProductFromList(P);
        }

         public void deleteEmployee(int teudatZeut)
        {
            Employee E = itsDAL.FindEmployeeByID(teudatZeut);
            itsDAL.deleteEmployeeFromList(E);
        }

         public void deleteClubMember(int teudatZeut)
        {
            ClubMember C = itsDAL.FindClubMemberByID(teudatZeut);
            itsDAL.deleteClubMemberFromList(C);
        }

         public void deleteTransaction(int transactionID)
        {
            Transaction T = itsDAL.FindTransactionByID(transactionID);
            itsDAL.deleteTransactionFromList(T);
        }

         public void updateProduct(int inventoryId, int stockCount)
         {
            itsDAL.updateProduct(inventoryId ,stockCount);

         }
         public List<Employee> GetAllEmployeesBySalaryRange(double salary1 , double salary2)
        {
            return itsDAL.GetEmployeeListBySalary(salary1 , salary2);
        }

         public List<ClubMember> GetClubMemberListByDateOfBirth(DateTime Age1, DateTime Age2)
        {
            return itsDAL.GetClubMemberListByDateOfBirth(Age1, Age2);
        }
        

        public List<ClubMember> GetAllClubMemberByClubID(int MemberID)
        {
            List<ClubMember> CM = new List<ClubMember>();
            CM = itsDAL.GetClubMemberByClubID(MemberID);

            return CM;
        }

          public List<Employee>  GetAllEmployeeListByDepID(int dpID)
        {
            List<Employee> Em = new List<Employee>();
            Em = itsDAL.GetEmployeeListByDepID(dpID);
            
                return Em;
          
        }

         public List<Transaction>  GetAllTransactionListByDate(DateTime date)
        {
            List<Transaction> Tr = new List<Transaction>();
            Tr = itsDAL.GetTransactiontListIsDate(date);
          
            return Tr;
        }

        public List<Transaction> GetTransactionListByPaymentMethod (PaymentMethod PaymentMethod )
         {
             List<Transaction> Tr = new List<Transaction>();
             Tr = itsDAL.GetTransactionListByPaymentMethod(PaymentMethod);

             return Tr;
         }
         public List<Product> GetProductListByType(Enum type)
        {
            List<Product> P = new List<Product>();
            P = itsDAL.GetProductListByType(type);

            return P;
        }
        public List<Product> GetProductListByPrice (double price1, double price2)
         {
             List<Product> P = new List<Product>();
             P = itsDAL.GetProductListByPrice(price1, price2);

             return P;
         }
        public List<Department> GetDepartmentByName(string name)
        {
            List<Department> D = new List<Department>();
            D = itsDAL.GetDepartmentByName(name);

            return D;
        }
       
        public int GetStockCountOfProduct(int inventoryID)
        {
            int counter = itsDAL.GetStockCountOfProduct(inventoryID);
            return counter;  
        }
    }

}

       
      