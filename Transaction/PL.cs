﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_backend;
using BL;

namespace PL
{
    public class PL_CLI : IPL
    {
        //The Command Line Interface Implimentation of the Presentation Layer 

        private IBL itsBL;

        public PL_CLI(IBL bl)// note that the constructor expects an IBL type (and not a specific implementaion specific)
        {
            itsBL = bl;
        }

        private void DisplayResult(List<Product> productList)
        {
            foreach (Product P in productList)
            {
                Console.WriteLine(P.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }

        private void DisplayResult(List<ClubMember> clubMemberList)
        {
            foreach (ClubMember C in clubMemberList)
            {
                Console.WriteLine(C.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }

        private void DisplayResult(List<Department> departmentList)
        {
            foreach (Department D in departmentList)
            {
                Console.WriteLine(D.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }



        private void DisplayResult(List<Employee> employeeList)
        {
            foreach (Employee E in employeeList)
            {
                Console.WriteLine(E.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }

        private void DisplayResult(List<Transaction> transactionList)
        {
            foreach (Transaction T in transactionList)
            {
                Console.WriteLine(T.ToString());
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.ReadLine();
        }


        public void Run()
        {
            bool dar = true;
            while (dar)
            {
                dar = isUser();
            }
        }

        public bool isUser()
        {
            Console.WriteLine("Welcome to the System. Please LogIn");
            Console.WriteLine("Enter your User Name:");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter you password");
            int pass;
            while (!int.TryParse(Console.ReadLine(), out pass))
            {
                Console.WriteLine("Invalid password. Try again (only numbers)");
            }


            return itsBL.FindUser(userName, pass);

        }
        private string ReceiveCmd(int i)
        {
            string st = Console.ReadLine();
            if (i == 1)//FirstQuestion &&  GetSetPatientDetails
            {
                while (st != "1" && st != "2" && st != "3" && st != "0")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 2)//ProductQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6" && st != "7")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
            if (i == 3)//TransQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
              if (i == 4)//CMQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6" && st != "7")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
                  if (i == 5)//EmpQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" && st != "6"  && st != "7")
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
                       if (i == 6)//DepQuestion
            {
                while (st != "1" && st != "2" && st != "3" && st != "0" && st != "4" && st != "5" )
                {
                    Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                    st = Console.ReadLine();
                }
                return st;
            }
                       if (i == 10)//id
                       {

                           bool flag = false;
                           while (!flag)
                           {
                               try
                               {
                                   int id = Convert.ToInt32(st);
                                   flag = st.Length == 9;
                                   if (!flag)
                                   {
                                       Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                       st = Console.ReadLine();
                                   }
                               }
                               catch (Exception e)
                               {
                                   Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                   st = Console.ReadLine();
                               }
                           }
                           return st;
                       }
                       if (i == 11)//name
                       {
                           return st;
                       }
                       if (i == 12)//gender
                       {
                           while (st != "male" && st != "Male" && st != "female" && st != "Female")
                           {
                               Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                               st = Console.ReadLine();
                           }
                           if (st == "male" || st == "Male")
                               return "Male";
                           else
                               return "Female";

                       }
                       if (i == 13)//salary
                       {

                           bool flag = false;
                           while (!flag)
                           {
                               try
                               {
                                   double sal = Convert.ToDouble(st);
                                   flag = sal > 0;
                                   if (!flag)
                                   {
                                       Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                       st = Console.ReadLine();
                                   }
                               }
                               catch (Exception e)
                               {
                                   Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                   st = Console.ReadLine();
                               }
                           }
                           return st;
                       }
                       if (i == 14)// day
                       {
                           bool flag = false;
                           while (!flag)
                           {
                               try
                               {
                                   int day = Convert.ToInt32(st);
                                   flag = day > 0 && day < 32;
                                   if (!flag)
                                   {
                                       Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                       st = Console.ReadLine();
                                   }
                               }
                               catch (Exception e)
                               {
                                   Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                   st = Console.ReadLine();
                               }
                           }
                           return st;

                       }
                       if (i == 15)// month
                       {
                           bool flag = false;
                           while (!flag)
                           {
                               try
                               {
                                   int month = Convert.ToInt32(st);
                                   flag = month > 0 && month < 13;
                                   if (!flag)
                                   {
                                       Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                       st = Console.ReadLine();
                                   }
                               }
                               catch (Exception e)
                               {
                                   Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                   st = Console.ReadLine();
                               }
                           }
                           return st;
                       }
                       if (i == 16)// year
                       {
                           bool flag = false;
                           while (!flag)
                           {
                               try
                               {
                                   int year = Convert.ToInt32(st);
                                   flag = year > 1000 && year < 9999;
                                   if (!flag)
                                   {
                                       Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                       st = Console.ReadLine();
                                   }
                               }
                               catch (Exception e)
                               {
                                   Console.WriteLine("you entered an invalid number or string ,please press enter and try again ");
                                   st = Console.ReadLine();
                               }
                           }
                           return st;

                       }
                     
                       return st;
            }

        public string reciveCMD (string type)
        {     
           while (!(productTypes.Cleaning.ToString()== type) && !(productTypes.Electronics.ToString()== type) && !(productTypes.Furniture.ToString()== type) && !(productTypes.None.ToString()== type) )
                {
                    Console.WriteLine("Invalid Product type. please try again : ");
                    type = Console.ReadLine();
                       }
           return type;
        }

        public string reciveCMD2 (string type)
        {
            while (!(PaymentMethod.AmericanExpress.ToString() == type) && !(PaymentMethod.Cash.ToString() == type) && !(PaymentMethod.Check.ToString() == type) && !(PaymentMethod.Isracard.ToString() == type) && !(PaymentMethod.MasterCard.ToString() == type) && !(PaymentMethod.Other.ToString() == type) && !(PaymentMethod.Visa.ToString() == type) )
            {
                Console.WriteLine("Invalid payment method. please try again : ");
                type = Console.ReadLine();
            }
            return type;
        }
    
        public void Run()
        {
            bool dar = true;
            while (dar)
            {
                dar = FirstQuestion();
            }
        }


        private bool FirstQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello and welcome to E-mart Department Store", "\n");
            Console.WriteLine("1 = Products in the store", "\n");
            Console.WriteLine("2 = Transactions", "\n");
            Console.WriteLine("3 = Club Member", "\n");
            Console.WriteLine("4 = Employee", "\n");
            Console.WriteLine("5 = Departments", "\n");
            Console.WriteLine("0 = Exit the system", "\n");
            Console.WriteLine("Please answer", "\n");
            return FirstQuestionAns(ReceiveCmd(1));
        }

        private bool FirstQuestionAns(string firstQuestion)
        {

            if (firstQuestion == "1")
            {
                ProductQuestion();
                return true;
            }

            if (firstQuestion == "2")
            {
                TransactionQuestion();
                return true;
            }

            if (firstQuestion == "3")
            {
                ClubMemberQuestion();
                return true;

            }

            if (firstQuestion == "4")
            {
                EmployeeQuestion();
                return true;

            }

            if (firstQuestion == "5")
            {
                DepartmentQuestion();
                return true;

            }
            if (firstQuestion == "0")
            {
                Console.Clear();
                Console.WriteLine("Thank you for using E-mart Department Store! :)");
                Console.WriteLine("press enter to exit the system");
                Console.ReadLine();
                return false;
            }
            return true;

        }

        private void ProductQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = add a new product", "\n");
            Console.WriteLine("2 = remove existing product", "\n");
            Console.WriteLine("3 = get data and update new data on existing product", "\n");
            Console.WriteLine("4 = get list of products by thier type", "\n");
            Console.WriteLine("5 = get list of products by thier price", "\n");
            Console.WriteLine("6 = get list of all the products", "\n");
            Console.WriteLine("7 = find a product by his InventoryID", "\n");
            Console.WriteLine("please answer:", "\n");
            ProductQuestionAns(ReceiveCmd(2));

        }

        private void ProductQuestionAns(string preQuestion)
        {
            if (preQuestion == "1")
             {
                 Console.Clear();
                 Console.WriteLine("please enter the product inventory ID number");
                 string invid = ReceiveCmd(10);
                 int Invid = Convert.ToInt32(invid);
                 while (!itsBL.FindProduct(Invid))
                 {
                     Console.WriteLine("the Product's ID you entered is not recognized by the system, please enter the correct ID");
                     invid = ReceiveCmd(10);
                     Invid = Convert.ToInt32(invid);
                 }
                 
                 Console.WriteLine("please enter the new product's name");
                 string name = ReceiveCmd(11);
                 Console.WriteLine("please enter the new product's type");
                 string type = reciveCMD(type);
                 productTypes Type = (productTypes)Enum.Parse(typeof((Type), "type");
                 Console.WriteLine("please enter the new product's location (9 didgit number )");
                 string location = ReceiveCmd(11);
                 int Location = Int32.Parse(location);
                 Console.WriteLine("please enter the new product's price");
                 string price = ReceiveCmd(13);
                 double Price = Convert.ToDouble(price);
                 itsBL.AddnewProduct(name,Type,Invid,Location,true,stock +1?,Price);// how to get the stock count and add 1?
             }

            if (preQuestion == "2")
             {
                 Console.Clear();
                 Console.WriteLine("please enter the product inventory ID number");
                 string invid = ReceiveCmd(10);
                 int Invid = Convert.ToInt32(invid);
                 while (!itsBL.FindProduct(Invid))
                 {
                     Console.WriteLine("the Product's ID you entered is not recognized by the system, please enter the correct ID");
                     invid = ReceiveCmd(10);
                     Invid = Convert.ToInt32(invid);
                 }
                itsBL.deleteProduct(Invid);
                 Console.WriteLine("The Product removed succsfully");

             }

            if (preQuestion == "3")
             {
                     Console.Clear();
                 Console.WriteLine("please enter the product inventory ID number");
                 string invid = ReceiveCmd(10);
                 int Invid = Convert.ToInt32(invid);
                 while (!itsBL.FindProduct(Invid))
                 {
                     Console.WriteLine("the Product's ID you entered is not recognized by the system, please enter the correct ID");
                     invid = ReceiveCmd(10);
                     Invid = Convert.ToInt32(invid);
                 }
                Console.WriteLine("please enter the Count of the product you want to update to");
                 string stockProduct = ReceiveCmd(4);
                 int stockProd = Convert.ToInt32(stockProduct);
                 while ( stockProd < 0 )
                      {
                     Console.WriteLine("the Product's Count you entered is not acceptable by the system, please enter the correct count of stock");
                    stockProduct = ReceiveCmd(4);
                    stockProd = Convert.ToInt32(stockProduct);
                 }

              itsBL.updateProduct(Invid, stockProd);  
             }

            if (preQuestion == "4")
             {
                 Console.Clear();
                 Console.WriteLine("please enter the product's type");
                 string type = reciveCMD(type);
                 productTypes Type = (productTypes)Enum.Parse(typeof((Type), "type");
                 DisplayResult(itsBL.GetProductListByType(Type));
             }

            if (preQuestion == "5")
            {
                Console.Clear();
                Console.WriteLine("please enter the product's minimal price");
                string min = ReceiveCmd(13);
                double Min = Convert.ToDouble(min);
                Console.WriteLine("please enter the product's maxmimal price");
                string max = ReceiveCmd(13);
                double Max = Convert.ToDouble(max);  
                DisplayResult(itsBL.GetProductListByPrice(Min,Max));
            }

            if (preQuestion == "6")
            {
                DisplayResult(itsBL.GetAllProductsList());
            }

            if (preQuestion == "7") 
            {
                Console.Clear();
                Console.WriteLine("please enter the product's 9 digit InventoryID number");
                 string prodID = ReceiveCmd(10);
                 int id = Convert.ToInt32(prodID);
                 while (!itsBL.FindProduct(id))
                 {
                     Console.WriteLine("the product's ID you entered is not recognized by the system, please enter the correct ID");
                     prodID = ReceiveCmd(10);
                     id = Convert.ToInt32(prodID);
                 }
                
            }

            if (preQuestion == "0")
             {
              return;
             }
        }

        private void TransactionQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if Transaction exist By ID", "\n");
            Console.WriteLine("2 = add new Transaction", "\n");
            Console.WriteLine("3 = delete Transaction", "\n");
            Console.WriteLine("4 = get all Transactions by thier date of issue", "\n");
            Console.WriteLine("5 = get all the Transactions by their Payment Methods", "\n");
            Console.WriteLine("6 = get all the Transactions", "\n");
            Console.WriteLine("Please answer", "\n");
            TransactionQuestionAns(ReceiveCmd(3));
        }

        private void TransactionQuestionAns(string tranQuestion)
        {
            if (tranQuestion == "1")
            {
                Console.Clear();
                Console.WriteLine("please enter the 9 digit Transaction ID number");
                string id = ReceiveCmd(10);
                int trID = Convert.ToInt32(id);
                bool exist = itsBL.FindTransactionByID(trID);
                if (bool){
                Console.WriteLine("The Transaction is exist in the in the system ");
                }
                else
                Console.WriteLine("Sorry, the Transaction is not exist in the in the system ");
        }

            ////important. to think a lot 
            //if (tranQuestion == "2")
            //{
            //    Console.Clear();
            //    Console.WriteLine("Please add a product to the transaction");
            //    string patId = ReceiveCmd(4);
            //    int PatientID = Convert.ToInt32(patId);
            //    DisplayResult(itsBL.GetAllPatientTreatments(PatientID));
            //}

            if (tranQuestion == "3")
            {
                Console.Clear();
                Console.WriteLine("please enter the 9 digit Transaction ID number", "\n");
                string tranId = ReceiveCmd(10);
                int TranID = Convert.ToInt32(tranId);
                bool exist = itsBL.FindTransactionByID(TranID);
                while (bool){
                Console.WriteLine("Sorry, The Transaction you entered is not exist in the in the system, please try again", "\n");
                 tranId = ReceiveCmd(10);
                 TranID = Convert.ToInt32(tranId);
                }
               
               itsBL.deleteTransaction(TranID);
            }

            if (tranQuestion == "4")
            {
                Console.Clear();
                Console.WriteLine("please enter the transaction's date of issue. ", "\n");
                Console.WriteLine("please enter the year");
                string year = ReceiveCmd(16);
                int Fyear = Convert.ToInt32(year);
                Console.WriteLine("please enter the month");
                string month = ReceiveCmd(15);
                int Fmonth = Convert.ToInt32(month);
                Console.WriteLine("please enter the day");
                string day = ReceiveCmd(14);
                int Fday = Convert.ToInt32(day);
                string all = string.Concat(year, month, day);
                DateTime Date = Convert.ToDateTime(all);
                DisplayResult(itsBL.GetAllTransactionListByDate(Date));
            }

            if (tranQuestion == "5")
            {
                Console.Clear();
                Console.WriteLine("please enter your the type of the payment method");
                string type = reciveCMD2(type); 
                PaymentMethod paymentMethod = (productTypes)Enum.Parse(typeof((paymentMethod), "type");
                DisplayResult(itsBL.GetTransactionListByPaymentMethod(paymentMethod));
            }

             if (tranQuestion == "6")
            {
                Console.Clear();
                DisplayResult(itsBL.GetAllTransactions());
            }
            if (tranQuestion == "0")
            {
             return;
            }
        }


        private void ClubMemberQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if a Club Member exist by his SSN ", "\n");
            Console.WriteLine("2 = Add a new Club Member", "\n");
            Console.WriteLine("3 = Remove a Club Member ", "\n");
            Console.WriteLine("4 = Get Club Members By their Date Of Birth range", "\n");
            Console.WriteLine("5 = Get a Club Member by his member ID number ", "\n");
            Console.WriteLine("6 = Get a list of all the Club Members ", "\n");
            Console.WriteLine("Please answer", "\n");
            ClubMemberQuesionAns(ReceiveCmd(4));
        }

        private void ClubMemberQuesionAns(string CMQuestion)
        {
            if (CMQuestion == "1")
                {
                Console.Clear();
                Console.WriteLine("please enter the club member's SSN (9 digit)");
                string Id = ReceiveCmd(10);
                int id = Convert.ToInt32(Id);
                bool exist = itsBL.FindTransactionByID(id);
                if (bool)
                Console.WriteLine("The Club Member is exist in the in the system ");
                
                else
                Console.WriteLine("Sorry, the Club Member is not exist in the in the system ");
                }

             if (CMQuestion == "2")
                {
                    Console.Clear();
                    Console.WriteLine("please enter the 9 digit member ID");
                    string memberID = ReceiveCmd(10);
                    int MemberID = Convert.ToInt32(memberID);
                   
                    Console.WriteLine("please enter the club member date of birth. ", "\n");
                     string year = ReceiveCmd(16);
                     int Fyear = Convert.ToInt32(year);
                 Console.WriteLine("please enter the month");
                 string month = ReceiveCmd(15);
                int Fmonth = Convert.ToInt32(month);
                Console.WriteLine("please enter the day");
                string day = ReceiveCmd(14);
                int Fday = Convert.ToInt32(day);
                string all = string.Concat(year, month, day);
                DateTime Date = Convert.ToDateTime(all);
                    Console.WriteLine("Please enter the member date of birth");
                    string date = ReceiveCmd(4);
                    DateTime enteredDate = DateTime.Parse(date); 
                    Console.WriteLine("please enter the member SSN");
                    string ssn = ReceiveCmd(4);
                    int SSN = Convert.ToInt32(ssn);
                    Console.WriteLine("please enter the club member first name");
                    string Fname = ReceiveCmd(4);
                    Console.WriteLine("please enter the club member last name");
                    string Lname = ReceiveCmd(4);
                    Console.WriteLine("please enter club member gender");
                    string gender = ReceiveCmd(4);
                    itsBL.AddnewClubMember(MemberID,enteredDate,SSN,Fname,Lname,gender);
                    Console.WriteLine("The club member added succussfuly");   
                }

              if (CMQuestion == "3")
                {
                    Console.Clear();
                    Console.WriteLine("please enter the club member SSN");
                    string ssn = ReceiveCmd(4);
                    int SSN = Convert.ToInt32(ssn);
                    itsBL.deleteClubMember(SSN);
                    Console.WriteLine("The club member removed succussfuly");   
                }
               if (CMQuestion == "4")
                {
                   Console.Clear();
                   Console.WriteLine("Please enter the 1st range club member date of birth");
                   string date = ReceiveCmd(4);
                   DateTime enteredDate = DateTime.Parse(date); 
                   Console.WriteLine("Please enter the 2nd range club member date of birth");
                    string date2 = ReceiveCmd(4);
                    DateTime enteredDate2 = DateTime.Parse(date2); 
                   DisplayResult(itsBL.GetClubMemberListByDateOfBirth(enteredDate, enteredDate2));
                }
               if (CMQuestion == "5")
               {
                   Console.WriteLine("please enter the club member ID");
                    string id = ReceiveCmd(4);
                    int ID = Convert.ToInt32(id);
                    DisplayResult(itsBL.GetAllClubMemberByClubID(ID));
               }
                if (CMQuestion == "6")
               {
                    DisplayResult(itsBL.GetAllClubMembers());
               }
                if (CMQuestion == "0")
            {
             return;
            }
        }

        private void EmployeeQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if an Employee exist by his SSN ", "\n");
            Console.WriteLine("2 = Add a new Employee", "\n");
            Console.WriteLine("3 = Remove an Employee ", "\n");
            Console.WriteLine("4 = Get an Employees by their salaries range", "\n");
            Console.WriteLine("5 = Get an Employees by their department ID number ", "\n");
            Console.WriteLine("6 = Get a list of all the Employees ", "\n");
            Console.WriteLine("Please answer", "\n");
            EmployeeQuestionAns(ReceiveCmd(5));
        }

        private void EmployeeQuestionAns(string EmpQuestion)
        {
            if (EmpQuestion == "1")
                {
                Console.Clear();
                Console.WriteLine("please enter the employee's SSN");
                string Id = ReceiveCmd(4);
                int id = Convert.ToInt32(Id);
                bool exist = itsBL.FindEmployeeByID(id);
                if (bool)
                Console.WriteLine("The Employee is exist in the in the system ");
                else
                Console.WriteLine("Sorry, the Employee is not exist in the in the system ");
                }

             if (EmpQuestion == "2")// check for doubles and validies
                {
                    Console.Clear();
                    Console.WriteLine("please enter the Employee salary");
                    string salary = ReceiveCmd(4);
                    int Salary = Convert.ToInt32(salary);
                    Console.WriteLine("Please enter the employee department ID");
                    string dep = ReceiveCmd(4);
                    int Dep = Convert.ToInt32(dep); 
                    Console.WriteLine("please enter the SSN of the supervisor of the employee");
                    string sup = ReceiveCmd(4);
                    int SUP = Convert.ToInt32(sup);
                    Console.WriteLine("please enter the employee's SSN");
                    string ssn = ReceiveCmd(4);
                    int SSN = Convert.ToInt32(ssn);
                    Console.WriteLine("please enter the employee first name");
                    string Fname = ReceiveCmd(4);
                    Console.WriteLine("please enter the employee last name");
                    string Lname = ReceiveCmd(4);
                    Console.WriteLine("please enter the employee gender");
                    string gender = ReceiveCmd(4);
                    itsBL.AddNewEmployee(Salary,Dep,SUP,SSN,Fname,Lname,gender);
                    Console.WriteLine("The Employee added succussfuly");   
                }

              if (EmpQuestion == "3")
                {
                    Console.Clear();
                    Console.WriteLine("please enter the employee SSN");
                    string ssn = ReceiveCmd(4);
                    int SSN = Convert.ToInt32(ssn);
                    bool exist = itsBL.FindEmployeeByID(SSN);
                    while (bool){
                    Console.WriteLine("Sorry, the Employee is not exist in the system, try again: ");
                    Console.WriteLine("please enter the employee SSN");
                    string ssn = ReceiveCmd(4);
                    int SSN = Convert.ToInt32(ssn);
                    }
                    itsBL.deleteEmployee(SSN);
                    Console.WriteLine("The Employee removed succussfuly");

               }
             
               if (EmpQuestion == "4")
                {
                   Console.Clear();
                   Console.WriteLine("Please enter the minimum salary ");
                    string min = ReceiveCmd(4);
                    double MIN = Convert.ToDouble(min);
                   Console.WriteLine("Please enter the maximum salary ");
                    string max = ReceiveCmd(4);
                    double MAX = Convert.ToDouble(max);
                   DisplayResult(itsBL.GetAllEmployeesBySalaryRange(MIN,MAX));
                }
               if (EmpQuestion == "5")
               {
                   Console.WriteLine("please enter the employee department ID number");
                    string id = ReceiveCmd(4);
                    int ID = Convert.ToInt32(id);
                    DisplayResult(itsBL.GetAllEmployeeListByDepID(ID));
               }
                if (EmpQuestion == "6")
               {
                    DisplayResult(itsBL.GetAllEmployees());
               }
                if (EmpQuestion == "0")
            {
             return;
            }
        }
        private void DepartmentQuestion()
        {
            Console.Clear();
            Console.WriteLine("Hello manager ", "\n");
            Console.WriteLine("to go back to the main menu press 0", "\n");
            Console.WriteLine("1 = Check if a department exist by it ID ", "\n");
            Console.WriteLine("2 = Add a new Deparment", "\n");
            Console.WriteLine("3 = Remove Department ", "\n");
            Console.WriteLine("4 = Get a department by it's name", "\n");
            Console.WriteLine("5 = Get a list of all the Employees ", "\n");
            Console.WriteLine("Please answer", "\n");
            DepartmentQuestionAns(ReceiveCmd(6));
        }

        private void DepartmentQuestionAns(string DepQuestion)
        {
            if (DepQuestion == "1")
                {
                Console.Clear();
                Console.WriteLine("please enter the department ID number");
                string Id = ReceiveCmd(4);
                int id = Convert.ToInt32(Id);
                bool exist = itsBL.FindDepartmentByID(id);
                if (bool)
                Console.WriteLine("The department is exist in the in the system ");
                else
                Console.WriteLine("Sorry, the department is not exist in the in the system ");
                }
             if (DepQuestion == "2")
                {
                Console.Clear();
                Console.WriteLine("please enter the new department ID");
                string Id = ReceiveCmd(4);
                int id = Convert.ToInt32(Id);
                Console.WriteLine("please enter the new department name");
                string name = ReceiveCmd(4);
                itsBL.AddNewDepartment(id,name);
                 Console.WriteLine("The department added succufully");
                }
             if (DepQuestion == "3")
                {
                Console.Clear();
                Console.WriteLine("please enter the department ID");
                string Id = ReceiveCmd(4);
                int id = Convert.ToInt32(Id);
                itsBL.deleteDepartment(id);
                Console.WriteLine("The department removed succufully");
                }
             if (DepQuestion == "4")
                {
                Console.Clear();
                Console.WriteLine("please enter the department name");
                string name = ReceiveCmd(4);
                 DisplayResult(itsBL.GetDepartmentByName(name));
               }
                if (DepQuestion == "5")
               {
                    DisplayResult(itsBL.GetAllDepartments());
               }
                if (DepQuestion == "0")
            {
             return;
            }

     
        }

    }
}

